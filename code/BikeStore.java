// Vladimir De-Vreeze, 2237501
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("RoadWheels", 14, 99.99);
        bikes[1] = new Bicycle("SpeedBikes", 28, 208.18);
        bikes[2] = new Bicycle("SlowNSteady", 2, 2.98);
        bikes[3] = new Bicycle("MountainExplorer", 198, 55.55);
        
        for(int i = 0; i < bikes.length; i++) {
            System.out.println(bikes[i].toString());
        }
    }
}
