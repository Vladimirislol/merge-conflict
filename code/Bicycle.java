// Vladimir De-Vreeze, 2237501
public class Bicycle {
    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    public String getManufacturer() {
        return this.manufacturer;
    }

    public int getNumGears() {
        return this.numberGears;
    }

    public double getMaxSpeed() {
        return this.maxSpeed;
    }

    public Bicycle(String manuf, int gears, double speed) {
        this.manufacturer = manuf;
        this.numberGears = gears;
        this.maxSpeed = speed;
    }

    public String toString() {
        String result = "Manufacturer: "+this.manufacturer+", Number of gears: "+this.numberGears+", Max Speed: "+this.maxSpeed;
        return result;
    }
}